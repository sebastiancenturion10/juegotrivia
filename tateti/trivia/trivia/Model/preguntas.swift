//
//  preguntas.swift
//  trivia
//
//  Created by Bootcamp 2 on 2022-10-28.
//

import Foundation

struct preguntas{
    
    
    var question: String = ""
    
    var vof = true
    var inicio = ""
    
    var preguntas = ["El número atómico del litio es 17": "falso" ,
                     "Hay cinco grupos sanguíneos diferentes": "falso",
                     "La caja negra de un avión es negra": "falso",
                     "La acrofobia es el miedo al ajo": "verdadero",
                     "El café está hecho de bayas": "verdadero",
                     "Meryl Streep ha ganado dos premios de la Academia": "falso",
                     "Waterloo tiene el mayor número de andenes de metro de Londres": "verdadero",
                     "La ginebra suele estar incluida en un Long Island Iced Tea": "verdadero",
                     "Hay 219 episodios de Friends": "falso"]



// la funcion verifica si es correcta o incorrecta la respuesta y cambia los colores de acuerdo a lo mismo:
//green: para correcto, red: para incorrecto
    mutating func corregir()-> Bool{
    for (myKey, myValue) in preguntas{
        if myKey == question && myValue == "verdadero"{
            vof = true
//                    label.textColor = UIColor.green
//                    label.text = "CORRECTO"
        }
        else if myKey == question && myValue == "falso"{
            vof = false
//                    label.textColor = UIColor.red
//                    label.text = "INCORRECTO"
        }
    }
    return vof
}
    
    mutating func iniciar()->String{
       // for (myKey, myValue) in preguntas{
            question = "\(preguntas.randomElement()!.key)"
            //label.text = "\(desafio.question)"
            //label.textColor = UIColor.black
       // }
        return question
    }

}
