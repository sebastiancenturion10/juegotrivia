//
//  ViewController.swift
//  trivia
//
//  Created by Bootcamp 2 on 2022-10-27.
//

import UIKit

class ViewController: UIViewController {
    
    var desafio = preguntas()

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var verdadero: UIButton!
    
    @IBOutlet weak var falso: UIButton!
    
    @IBOutlet weak var jugar: UIButton!
    
//    var question: String = ""
//
//    var vof = true
//
//    var preguntas = ["El número atómico del litio es 17": "falso" ,
//                     "Hay cinco grupos sanguíneos diferentes": "falso",
//                     "La caja negra de un avión es negra": "falso",
//                     "La acrofobia es el miedo al ajo": "verdadero",
//                     "El café está hecho de bayas": "verdadero",
//                     "Meryl Streep ha ganado dos premios de la Academia": "falso",
//                     "Waterloo tiene el mayor número de andenes de metro de Londres": "verdadero",
//                     "La ginebra suele estar incluida en un Long Island Iced Tea": "verdadero",
//                     "Hay 219 episodios de Friends": "falso"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Siguiente(_ sender: UIButton) {
        
        // la funcion verifica si es correcta o incorrecta la respuesta y cambia los colores de acuerdo a lo mismo:
        //green: para correcto, red: para incorrecto
//        func corregir(){
//            for (myKey, myValue) in preguntas{
//                if myKey == question && myValue == "verdadero"{
//                        vof = true
////                    label.textColor = UIColor.green
////                    label.text = "CORRECTO"
//                }
//                else if myKey == question && myValue == "falso"{
//                    vof = false
////                    label.textColor = UIColor.red
////                    label.text = "INCORRECTO"
//                }
//            }
//        }
        
        
        var valor = ""
        
        //si el sender tag es 3, aparece una pregunta en color negro
        if sender.tag == 3{

            valor = "\(desafio.iniciar())"
            print(valor)
            label.text = "\(valor)"
            label.textColor = UIColor.black
            desafio.question = valor
//            for (myKey, myValue) in desafio.preguntas{
//                desafio.question = "\(desafio.preguntas.randomElement()!.key)"
//                label.text = "\(desafio.question)"
//                label.textColor = UIColor.black
//            }
        }
        
        
        
        
        //si oprime el boton "verdadero"
        if sender.tag == 1 {
            if desafio.corregir() == true{
            label.text = "CORRECTO"
            label.textColor = UIColor.green}
            
            else if desafio.corregir() == false {
                label.text = "INCORRECTO"
                label.textColor = UIColor.red
                }
            
        }
        
        
        //si oprime el boton "falso"
        if sender.tag == 2 {
            if desafio.corregir() == false{
            label.text = "CORRECTO"
            label.textColor = UIColor.green
            }
            else if desafio.corregir() == true{
            label.text = "INCORRECTO"
            label.textColor = UIColor.red}
        }
    }
}

