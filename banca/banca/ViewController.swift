//
//  ViewController.swift
//  banca
//
//  Created by Bootcamp 2 on 2022-10-31.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var sexo: UITextField!
    
    
    @IBOutlet weak var estadoCivil: UITextField!
    
    
    @IBOutlet weak var celular: UITextField!
    
    
    @IBOutlet weak var correo: UITextField!
    
    
    @IBOutlet weak var correo2: UITextField!
    
    
    @IBOutlet weak var fechaNacimiento: UIDatePicker!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func siguiente(_ sender: Any) {
        //se captura el valor del text field en un array
        var getCel = celular.text!
        
        // convertir el numero de telefono a un array
        var numeroDeTel = Array(getCel)
        
        
        //validar que el campo no este vacio
        if numeroDeTel.isEmpty{
            celular.attributedPlaceholder = NSAttributedString(string:"Este campo es obligatorio",
                                                               attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
        }

        
        //validar que el numero sea de diez digitos
        else if numeroDeTel.count == 10 && numeroDeTel[0] != "0" || numeroDeTel[1] != "9"{
            celular.text = ""
            celular.attributedPlaceholder = NSAttributedString(string:"El numero no es valido",
                                                               attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
        }

        
        var getCorreo = correo.text!
        var email = Array(getCorreo)
        
        //validar que el campo no este vacio
        if email.isEmpty{
            correo.attributedPlaceholder = NSAttributedString(string:"Este campo es obligatorio",
                                                               attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
        }
        
        //validar que el correo tenga fromato de correo electronico
        
        func isValidEmail(_ email: String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: email)
        }
        
        if isValidEmail(getCorreo){
            correo2.text = "\(getCorreo)"
        }
        else {
            correo.text = ""
            correo.attributedPlaceholder = NSAttributedString(string:"El correo no es valido",
                                                               attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
           
            //texto que se muestra en el campo de validacion de correo
            correo2.text = ""
            correo2.attributedPlaceholder = NSAttributedString(string:"El correo no es valido",
                                                               attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
            
            
        }
    }
}

